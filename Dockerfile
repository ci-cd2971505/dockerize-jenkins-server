#Importing base image Ubuntu
FROM ubuntu:22.04
ENV DEBIAN_FRONTEND noninteractive
#Updating and Upgrading Ubuntu
RUN apt-get -y update
#Installing Basic Packages & Utilities in Ubuntu
RUN apt-get -y install software-properties-common git gnupg sudo nano vim wget curl zip unzip build-essential libtool autoconf uuid-dev pkg-config libsodium-dev lynx-common tcl inetutils-ping net-tools ssh openssh-server openssh-client openssl letsencrypt apt-transport-https telnet locales gdebi lsb-release
#Clear cache
RUN apt-get clean
#Jenkins Prerequisites
RUN sudo apt search openjdk
#Install Java version 11 as prerequisite
RUN apt-get -y install openjdk-11-jdk
RUN apt-get -y install openjdk-17-jdk openjdk-17-jre
#Jenkins installation
#Download & add repository key
RUN curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key | sudo tee \
 /usr/share/keyrings/jenkins-keyring.asc > /dev/null
#Getting binary file into /etc/apt/sources.list.d
RUN echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null
#Updating packages
RUN sudo apt-get -y update
#Installing Jenkins
RUN sudo apt-get -y install jenkins
#Install Maven
RUN sudo apt-get -y install maven
#Start jenkins
RUN service jenkins start
#Expose port 8080
EXPOSE 8080
#
ENTRYPOINT service jenkins start && /bin/bash
